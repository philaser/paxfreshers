from app import app
import datetime, random, string
from sqlalchemy_searchable import SearchQueryMixin, make_searchable
from sqlalchemy_utils.types import TSVectorType
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from flask_bcrypt import Bcrypt
from flask_wtf import FlaskForm
from wtforms import ValidationError, validators, widgets, IntegerField, BooleanField, StringField, PasswordField, RadioField, SelectField, HiddenField



db = SQLAlchemy()
bcrypt = Bcrypt()

make_searchable()

class StudentQuery(BaseQuery, SearchQueryMixin):
    pass
 
class Student(db.Model):
    query_class = StudentQuery

    f_name = db.Column(db.Unicode(30), nullable = False)
    l_name = db.Column(db.Unicode(30), nullable = False)
    sex    = db.Column(db.Unicode,nullable = False)
    ref_no = db.Column(db.Unicode, primary_key = True)
    tel_no = db.Column(db.Unicode(10), unique = True, nullable = False)
    course = db.Column(db.Unicode)
    hall = db.Column(db.Unicode)
    room_no = db.Column(db.Unicode)
    block = db.Column(db.Unicode)
    year = db.Column(db.Unicode)
    search_vector = db.Column(TSVectorType('block', 'f_name', 'hall', 'l_name', 'ref_no', 'sex', 'tel_no','room_no','year', 'course'))
    
    def __init__(self, f_name, l_name, sex, ref_no, tel_no, hall, room_no, block, course):
        self.f_name = f_name
        self.l_name = l_name
        self.sex    = sex
        self.ref_no = ref_no
        self.tel_no = tel_no
        self.course = course
        self.hall   = hall
        self.room_no = room_no
        self.block = block
        self.year = str(datetime.date.today().year)

    def __repr__(self):
        return  self.ref_no

db.configure_mappers()

class User(db.Model):
    

    id = db.Column(db.String, primary_key = True)
    password = db.Column(db.String)
    type = db.Column(db.String)
    email = db.Column(db.String)

    def __init__(self, acc_type, email):
        self.id = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(6))
        self.pword = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(10))
        self.password = bcrypt.generate_password_hash(self.pword)
        self.type  = acc_type
        self.email = email
    def __repr__(self):
        return "User ID: " + self.id + "Type: " + self.type




class StudentForm(FlaskForm):
    id = StringField('id', widget= widgets.Input(input_type='hidden'))
    f_name = StringField('first name', validators=[validators.DataRequired()])
    l_name = StringField('last name', validators = [validators.DataRequired()])
    sex = RadioField('sex', choices=[('male','Male'),('female','Female')], validators = [validators.DataRequired()])
    ref_no = IntegerField('reference number', validators = [validators.DataRequired()])
    tel_no = StringField('telephone', validators = [validators.DataRequired(), validators.Length(min=10, max=10, message='Incorrect Telephone number pattern')])
    course = StringField('Course:')
    hall = SelectField('Hall/Hostel:',choices=[('Unity', 'Unity'), ('Independence', 'Independence'), ('Republic', 'Republic'), ('Queen Elizabeth II','Queen Elizabeth II'), ('Africa','Africa'), ('University','University'), ('Brunei', 'Brunei'), ('Hall 7', 'Hall 7')], validators = [validators.DataRequired()])
    room_no = StringField('Room:')
    block = SelectField('Block:', choices=[('-', '-'), ('A', 'A'), ('B', 'B'), ('Main', 'Main'), ('Annex', 'Annex')], validators = [validators.DataRequired()])


    def validate_ref_no(form, field):
        student =Student.query.filter_by(ref_no=str(field.data)).first()
        
        if 'id' in form:
            id = form.id.data
        else:
            id = ""
            
        if id == "":
            if student:
                raise ValidationError("Reference number exists!") 
        else:
            if student and str(id) != str(student.ref_no):
                raise ValidationError("Reference number exists!")



    def validate_tel_no(form, field):
        student =Student.query.filter_by(tel_no=str(field.data)).first()
        
        if 'id' in form:
            id = form.id.data
        else:
            id = ""
            
        if id is "":
            if student:
                raise ValidationError("Telephone number exists!") 
        else:
            if student and str(id) != str(student.ref_no):
                raise ValidationError("Telephone number exists!")