from app import app
import os, datetime
from functools import wraps
from flask import url_for, request, render_template,flash,redirect,get_flashed_messages,session
from models import db, Student, User, bcrypt, StudentForm
from flask_mail import Mail, Message
from sqlalchemy import or_
import flask_excel as excel

app.secret_key = "1998"
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL'] 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'paxfreshers@gmail.com'
app.config['MAIL_PASSWORD'] = 'MotherMary'
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_DEFAULT_SENDER'] = "paxfreshers@gmail.com"
mail = Mail(app)
db.init_app(app)
excel.init_excel(app)

admin_username = "admin"
admin_password = "admin"
items_per_page = 50

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'username' not in session:
            flash("Login to access this page","danger")
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

def check_acc(acc_type):
    def login_required(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if 'username' not in session or acc_type != session['acc_type']:
                flash("Login to Account authorized to access this page","danger")
                return redirect(url_for('login', next=request.url))
            return f(*args, **kwargs)
        return decorated_function
    return login_required

@app.route('/',methods=['POST','GET'])
def index():
    if 'username' in session:
        flash("logged in as "+ session["acc_type"],"success")
        if session['acc_type'] == 'admin':
            return redirect(url_for('admin'))
        elif session['acc_type'] == 'collector':
            return redirect(url_for('add'))
        else:
            return redirect(url_for('view'))
    else:
        return redirect(url_for('login'))

@app.route('/login', methods=['POST','GET'])
def login():
    url = request.args.get("next", "")
    if request.method == "POST":
        user = User.query.filter_by(id=request.form['username']).first()
        if request.form['username'] == admin_username and request.form['password'] == admin_password:
            session['username'] = request.form["username"]
            session['acc_type'] = "admin"
            if url == "":
                url = url_for('admin')
            flash("You have succesfuly been logged in","success")
            return redirect(url)
        elif user is not None:
            if request.form['username'] == user.id and bcrypt.check_password_hash(user.password, request.form['password']):
                session['username'] = request.form["username"]
                session['acc_type'] = user.type
                if url == "":
                    if user.type == "collector":
                        url = url_for('add')
                    else:
                        url = url_for('view')
                flash("You have succesfuly been logged in","success")
                return redirect(url)
        else:
            flash('Error: invalid credentials','danger')
            if url == "":
                url = url_for("login")
            return redirect(url)
    return render_template('login.html')

@app.route('/logout')
def logout():
    session.pop('username', None)
    flash("you have been logged out","success")
    return redirect(url_for('login'))


@app.route('/add', methods=['POST', 'GET'])
@check_acc("collector")
def add():
    form = StudentForm()
    if request.method == 'POST' and form.validate_on_submit():
        fname = form.f_name.data.title()
        lname = form.l_name.data.title()
        room = form.room_no.data.upper()
        if form.block.data == '-': #coerceing a null value for block data
            form.block.data = None
        if form.room_no.data == "": #coerceing a null value for room_no data
            form.room_no.data = None

        student = Student(fname, lname, form.sex.data, form.ref_no.data,
        form.tel_no.data,form.hall.data,room,form.block.data, form.course.data)
        db.session.add(student)
        db.session.commit()
        flash(fname + " " + lname + " succesfully added!", "success")
        return redirect(url_for("add"))

    return render_template('add.html', form=form)

@app.route('/view', methods=['POST','GET'])
@app.route('/view/<int:page>', methods=['POST','GET'])
@check_acc("viewer")
def view(page=1):
    studentlist = Student.query.order_by(Student.f_name.asc(),Student.l_name.asc()).paginate(page, items_per_page, False)
    
    if 'column' in request.args:
        column = request.args.get('column','')
        if request.args.get('order','') == 'desc':
            studentlist = Student.query.order_by(getattr(Student, column).desc(), Student.f_name.desc(), Student.l_name.desc()).paginate(page, items_per_page, False)
        else:
            studentlist = Student.query.order_by(getattr(Student, column).asc(), Student.f_name.asc(), Student.l_name.asc()).paginate(page, items_per_page, False)
    if 'search' in request.args:
        if request.args.get('search','') != "":
            query = Student.query.search(request.args.get('search','')).paginate(page, items_per_page, False)
            studentlist = query
            if not query:
                flash("nothing found!","warning")
                studentlist = Student.query.all()
    
    action = request.args.get('action','')
    id= request.args.get('id','')
    student = Student.query.filter_by(ref_no = id).first()
    form = StudentForm(obj=student)

    if request.method == "POST" and form.validate_on_submit():
        
        room = form.room_no.data.upper()

        if form.block.data == '-': #coerceing a null value for block data
           form.block.data = None
        if form.room_no.data == "": #coerceing a null value for room_no data
            room = None

        student.f_name = form.f_name.data.title()
        student.l_name = form.l_name.data.title()
        student.sex = form.sex.data
        student.ref_no = form.ref_no.data
        student.tel_no = form.tel_no.data
        student.hall = form.hall.data
        student.room_no = str(room)
        student.block = form.block.data
        student.year = str(student.year)
        student.course = form.course.data

        db.session.commit()
        flash("Updated!", "success")
        return redirect(url_for('view'))

    if action == "edit":
        return render_template('edit.html', form=form, id=id)

    if action == "delete":
        db.session.delete(student)
        db.session.commit()
        flash("deleted!","success")
        return redirect(url_for('view'))


    return render_template('view.html',studentlist=studentlist, q=request.args.get('search',''))


@app.route('/admin', methods=['POST','GET'])
@check_acc("admin")
def admin():
    return render_template("admin.html")

@app.route('/create',methods=['POST', 'GET'])
@app.route('/create/<int:page>',methods=['POST', 'GET'])
@check_acc("admin")
def create(page = 1):
    if request.method == "POST":
        email = request.form['email_field']
        if request.form['submit_button'] == "Create viewer":
            create_user("viewer", email)
            flash("Sent viewer credentials to "+ email,"success")
            return redirect(url_for('admin'))
        elif request.form['submit_button'] == "Create collector":
            create_user("collector", email)
            flash("Sent collector credentials to "+ email,"success")
            return redirect(url_for('admin'))
    
    if request.method == "GET":
        response = request.args.get("action","")
        if response == "delete":
            list = User.query.paginate(page, items_per_page, False)
            return render_template("userlist.html", list=list)


@app.route("/delete")
@check_acc("admin")
def delete():
    idgot = request.args.get('id', '')
    User.query.filter_by(id=idgot).delete()
    db.session.commit()
    return redirect(url_for("create", action="delete"))



@app.route("/wtftest", methods=['GET','POST'])
@login_required
def wtftest():
    student =Student.query.filter_by(ref_no="20406464").first()
    print(student.f_name)
    form = StudentForm(obj=student)
    return render_template("add.html", form=form)

@app.route("/export", methods=['GET'])
@check_acc("admin")
def export():
    query = Student.query.all()
    column_names = ['ref_no', 'f_name', 'l_name', 'sex', 'tel_no', 'course', 'hall', 'room_no', 'block', 'year']
    return excel.make_response_from_query_sets(query, column_names, "xls", file_name="Database-" + str(datetime.date.today()) )
    #return excel.make_response_from_tables(db.session, [Student], "xls", file_name="Database")








def create_user(acc_type, email):
    try:
        with app.app_context():
            u = User(acc_type, email)
            user = User.query.filter_by(id=u.id).first()
            if user:
                return create_user(acc_type, email)
            db.session.add(u)
            db.session.commit()
            message = "Login details for "+ u.type + " account"
            msg = Message(message, recipients=[email])
            msg.body = "your id is: " + u.id + " your password is: " + u.pword
            mail.send(msg)
    except RuntimeError:
        print("Cant create any more unique ids. delete some old ones first.")


