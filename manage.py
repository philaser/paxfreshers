from server import app
from models import db, User, Student, bcrypt


def create_db():
    with app.app_context():
        db.create_all()
    return 'done!'

def create_user(acc_type, email):
    try:
        with app.app_context():
            u = User(acc_type, email)
            user = User.query.filter_by(id=u.id).first()
            if user:
                return create_user(acc_type, email)
            db.session.add(u)
            db.session.commit()
            print(u.pword)
            if bcrypt.check_password_hash(u.password, u.pword):
                print("password match!")
        return 'done!'
    except RuntimeError:
        print("Cant create any more unique ids. delete some old ones first.")